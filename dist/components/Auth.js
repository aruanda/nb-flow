/*globals $:false*/
'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

module.exports = function (requester, callWhenAuthenticated) {
  return {
    componentDidMount: function () {
      var loginApp = ReactDOM.findDOMNode(this);
      $(loginApp).attr('class', 'login-box');
      $('body').attr('class', 'hold-transition login-page');

      setTimeout(function () {
        $.AdminLTE.layout.activate();
        $(loginApp).find('input[type="checkbox"]').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    },

    getInitialState: function () {
      return {
        error: false,
        waiting: false
      };
    },

    handleTryAuth: function (e) {
      e.preventDefault();
      var state = this.state;

      state.error = false;
      state.waiting = false;

      var email = (this.refs.email.value || '').trim();
      var pass = (this.refs.password.value || '').trim();

      var isEmptyEmail = !email;
      if (isEmptyEmail) {
        state.error = 'auth.error.empty.email'.translate();
        this.setState(state);
        return false;
      }

      var mathEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var isInvalidEmail = !mathEmail.test(email);
      if (isInvalidEmail) {
        state.error = 'auth.error.invalid.email'.translate();
        this.setState(state);
        return false;
      }

      var isEmptyPassword = !pass;
      if (isEmptyPassword) {
        state.error = 'auth.error.empty.password'.translate();
        this.setState(state);
        return false;
      }

      state.waiting = true;
      this.setState(state, function () {
        var params = { email: email, password: pass };

        requester.getInstance().post(params).then(callWhenAuthenticated).catch(this.failAuth);
      }.bind(this));
      return false;
    },

    failAuth: function (err) {
      var state = this.state;
      var error = err.response.body;

      state.error = error;
      state.waiting = false;
      this.setState(state);
    },

    render: function () {
      var isWait = this.state.waiting;
      var isError = this.state.error;

      var error;
      var waiting;

      if (isError) error = React.createElement(
        'div',
        { className: 'alert alert-danger alert-dismissible' },
        React.createElement(
          'button',
          { type: 'button', className: 'close', 'data-dismiss': 'alert', 'aria-hidden': 'true' },
          '×'
        ),
        React.createElement(
          'h4',
          null,
          React.createElement('i', { className: 'icon fa fa-ban' }),
          ' Atenção!'
        ),
        this.state.error
      );

      if (isWait) waiting = React.createElement(
        'div',
        { className: 'overlay' },
        React.createElement('i', { className: 'fa fa-refresh fa-spin' })
      );

      return React.createElement(
        'div',
        { className: 'login-box' },
        React.createElement(
          'div',
          { className: 'login-logo' },
          React.createElement(
            'b',
            null,
            'nbA'
          ),
          'dmin'
        ),
        React.createElement(
          'div',
          { className: 'box' },
          React.createElement(
            'div',
            { className: 'box-body' },
            React.createElement(
              'div',
              { className: 'login-box-body' },
              React.createElement(
                'p',
                { className: 'login-box-msg' },
                'login.label.enter'.translate()
              ),
              React.createElement(
                'form',
                { role: 'form', onSubmit: this.handleTryAuth },
                error,
                React.createElement(
                  'div',
                  { className: 'form-group has-feedback' },
                  React.createElement('input', {
                    ref: 'email',
                    type: 'email',
                    className: 'form-control',
                    placeholder: 'Email'
                  }),
                  React.createElement('span', { className: 'glyphicon glyphicon-envelope form-control-feedback' })
                ),
                React.createElement(
                  'div',
                  { className: 'form-group has-feedback' },
                  React.createElement('input', {
                    ref: 'password',
                    type: 'password',
                    className: 'form-control',
                    placeholder: 'Password'
                  }),
                  React.createElement('span', { className: 'glyphicon glyphicon-lock form-control-feedback' })
                ),
                React.createElement(
                  'div',
                  { className: 'row' },
                  React.createElement(
                    'div',
                    { className: 'col-xs-4' },
                    React.createElement(
                      'button',
                      { type: 'submit', className: 'btn btn-primary btn-block btn-flat' },
                      'login.btn.enter'.translate()
                    )
                  )
                )
              )
            )
          ),
          waiting
        )
      );
    }
  };
};