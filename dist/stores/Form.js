'use strict';

var Modes = require('../Modes');

module.exports = function (pActions, requester) {
  return {
    listenables: [pActions],

    state: {
      mode: Modes.new,
      model: {},
      error: false,
      waiting: false
    },

    whenSuccessLoadRequest: function (res) {
      var state = this.state;
      if (!res.body.rows) throw { response: { body: pActions.concat('.error.not.found') } };

      var model = res.body.data.pop();

      state.mode = Modes.loaded;
      state.model = model;
      state.waiting = false;
    },

    onAdd: function () {
      var store = this;
      var state = store.state;

      if (!state.model) return;
      if (!state.model.id) return;

      state.model = {};
      state.mode = Modes.loaded;
      state.waiting = false;
      store.triggerAsync(store);
    },

    onLoad: function (code) {
      var store = this;
      var state = store.state;

      state.mode = Modes.edit, state.error = false;
      state.waiting = true;
      store.triggerAsync(store);

      var params = {
        conditions: { id: code },
        limit: 1
      };

      return requester.getInstance().get(params).then(store.whenSuccessLoadRequest).catch(store.whenErrorRequest).then(store.viewRender);
    },

    getInitialState: function () {
      return this.state;
    },

    whenSuccessRequest: function (res) {
      var state = this.state;
      var model = res.body;

      state.model = model;
      state.waiting = false;
    },

    whenErrorRequest: function (err) {
      var state = this.state;
      var error = err.response.body;

      state.error = error;
      state.waiting = false;
    },

    viewRender: function () {
      var store = this;
      var state = store.state;
      store.trigger(state);
    },

    onSave: function (model) {
      var store = this;
      var state = store.state;
      var update = Boolean(model && model.hasOwnProperty('id') && model.id);

      state.mode = update ? Modes.savingEdit : Modes.savingNew;
      state.model = model;
      state.error = false;
      state.waiting = true;
      store.triggerAsync(store);

      var method = update ? 'put' : 'post';

      return requester.getInstance()[method](model).then(store.whenSuccessRequest).catch(store.whenErrorRequest).then(store.viewRender);
    }
  };
};