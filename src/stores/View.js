'use strict';

var Modes = require('../Modes');

module.exports = function(pActions, requester) {
  return {
    listenables: [pActions],

    state: {
      mode: Modes.view,
      model: {},
      error: false,
      waiting: false
    },

    getInitialState: function() {
      return this.state;
    },

    whenSuccessRequest: function(res) {
      var state = this.state;
      if (!res.body.rows) throw { response: { body: 'error.not.found' } };

      var model = res.body.data.pop();

      state.mode    = Modes.loaded;
      state.model   = model;
      state.waiting = false;
    },

    whenErrorRequest: function(err) {
      var state = this.state;
      var error = err.response.body;

      state.error   = error;
      state.waiting = false;
    },

    viewRender: function() {
      var store = this;
      var state = store.state;
      store.trigger(state);
    },

    onLoad: function(code) {
      var store = this;
      var state = store.state;

      state.error   = false;
      state.waiting = true;
      store.triggerAsync(store);

      var params = {
        conditions: { id: code },
        limit: 1
      };

      return requester
        .getInstance()
        .get(params)
        .then(store.whenSuccessRequest)
        .catch(store.whenErrorRequest)
        .then(store.viewRender);
    }
  };
};
