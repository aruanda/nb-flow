'use strict';

var nbListStore = require('./stores/List');
var nbFormStore = require('./stores/Form');
var nbViewStore = require('./stores/View');

var nbListActions = require('./actions/List');
var nbFormActions = require('./actions/Form');
var nbViewActions = require('./actions/View');

var nbAuthPage = require('./components/Auth');
var nbListPage = require('./components/List');
var nbFormPage = require('./components/Form');
var nbViewPage = require('./components/View');

var nbLinks = require('./links');
var nbFields = require('./NurimbaFields');

module.exports = {
  nbLinks: nbLinks,
  nbFields: nbFields,

  nbListStore: nbListStore,
  nbFormStore: nbFormStore,
  nbViewStore: nbViewStore,

  nbListActions: nbListActions,
  nbFormActions: nbFormActions,
  nbViewActions: nbViewActions,

  nbAuthPage: nbAuthPage,
  nbListPage: nbListPage,
  nbFormPage: nbFormPage,
  nbViewPage: nbViewPage
};