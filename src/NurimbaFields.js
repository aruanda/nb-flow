'use strict';

var lodash = require('lodash');

module.exports = function(domain, fields) {
  return function(filter) {
    var sorting = 'orderPreview';

    if (filter === 'viewForm') sorting = 'orderForm';
    if (filter === 'viewList') sorting = 'orderList';

    var listFields = lodash.cloneDeep(fields).filter(function(field) {
      return field[filter];
    }).map(function(field) {
      field.domain = domain;
      return field;
    });

    return lodash.sortBy(listFields, sorting);
  };
};
