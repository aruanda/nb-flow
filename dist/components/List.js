'use strict';

var React = require('react');
var nbHistory = require('react-router').browserHistory;
var ListContent = require('nb-list');

module.exports = function (pDomain, cols, pStore, pActions, links, buttons) {
  var Reflux = require('reflux');

  return {
    mixins: [Reflux.connect(pStore, 'store')],

    goToViewRecord: function (record) {
      nbHistory.push(links.viewById(record.id));
    },

    goToEditRecord: function (record) {
      nbHistory.push(links.editById(record.id));
    },

    componentDidMount: function () {
      pActions.search();
    },

    getActionsList: function () {
      var actionsList = {
        btns: [{
          text: pDomain.concat('.list.view'),
          type: 'button',
          onClick: this.goToViewRecord,
          className: 'btn btn-block btn-default btn-sm',
          icon: 'glyphicon glyphicon-eye-open'
        }, {
          text: pDomain.concat('.list.edit'),
          type: 'button',
          onClick: this.goToEditRecord,
          className: 'btn btn-block btn-default btn-sm',
          icon: 'glyphicon glyphicon-edit'
        }]
      };

      return actionsList;
    },

    render: function () {
      var store = this.state.store;
      var actions = this.getActionsList();

      if (!buttons) buttons = [];

      return React.createElement(
        'div',
        { className: 'content-wrapper' },
        React.createElement(
          'section',
          { className: 'content-header overflowHidden' },
          React.createElement(
            'h1',
            { className: 'pull-left' },
            pDomain.concat('.list.title').translate()
          ),
          React.createElement(
            'ul',
            { className: 'list-unstyled pull-right las' },
            buttons.filter(function (btn) {
              return Boolean(!btn.hasOwnProperty('viewOnly') || !btn.viewOnly || btn.viewOnly());
            }).map(function (btn, index) {
              return React.createElement(
                'li',
                { key: index, className: 'pull-left marginLeft10px' },
                React.createElement(
                  'button',
                  { className: btn.className, type: btn.type, onClick: function () {
                      btn.onClick();
                    } },
                  btn.text.translate()
                )
              );
            })
          )
        ),
        React.createElement(
          'section',
          { className: 'content' },
          React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
              'div',
              { className: 'col-xs-12' },
              React.createElement(
                'div',
                { className: 'box' },
                React.createElement(
                  'div',
                  { className: 'box-body table-responsive no-padding' },
                  React.createElement(ListContent, {
                    cols: cols,
                    data: store.rows,
                    loaded: store.fetch,
                    actions: actions
                  })
                )
              )
            )
          )
        )
      );
    }
  };
};