'use strict';

module.exports = function (pActions, requester) {
  return {
    listenables: [pActions],

    state: {
      count: 0,
      rows: [],
      fetch: true,
      error: false
    },

    getInitialState: function () {
      return this.state;
    },

    whenSuccessRequest: function (res) {
      var state = this.state;
      var resultSet = res.body;

      state.fetch = true;
      state.count = resultSet.rows;
      state.rows = resultSet.data;
    },

    whenErrorRequest: function (err) {
      var state = this.state;
      var error = err.response.body;

      state.error = error;
      state.fetch = true;
      state.count = 0;
      state.rows = [];
    },

    viewRender: function () {
      var store = this;
      var state = store.state;
      store.trigger(state);
    },

    onSearch: function (params) {
      var store = this;
      var state = store.state;

      state.fetch = false;
      state.error = false;
      store.triggerAsync(state);

      return requester.getInstance().get(params).then(store.whenSuccessRequest).catch(store.whenErrorRequest).then(store.viewRender);
    }
  };
};