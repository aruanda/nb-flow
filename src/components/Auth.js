/*globals $:false*/
'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

module.exports = function(requester, callWhenAuthenticated) {
  return {
    componentDidMount: function() {
      var loginApp = ReactDOM.findDOMNode(this);
      $(loginApp).attr('class', 'login-box');
      $('body').attr('class', 'hold-transition login-page');

      setTimeout(function() {
        $.AdminLTE.layout.activate();
        $(loginApp).find('input[type="checkbox"]').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    },

    getInitialState: function() {
      return {
        error: false,
        waiting: false
      };
    },

    handleTryAuth: function(e) {
      e.preventDefault();
      var state = this.state;

      state.error   = false;
      state.waiting = false;

      var email = (this.refs.email.value     || '').trim();
      var pass  = (this.refs.password.value  || '').trim();

      var isEmptyEmail = !email;
      if (isEmptyEmail) {
        state.error = 'auth.error.empty.email'.translate();
        this.setState(state);
        return false;
      }

      var mathEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var isInvalidEmail = !mathEmail.test(email);
      if (isInvalidEmail) {
        state.error = 'auth.error.invalid.email'.translate();
        this.setState(state);
        return false;
      }

      var isEmptyPassword = !pass;
      if (isEmptyPassword) {
        state.error = 'auth.error.empty.password'.translate();
        this.setState(state);
        return false;
      }

      state.waiting = true;
      this.setState(state, function() {
        var params = { email: email, password: pass };

        requester
          .getInstance()
          .post(params)
          .then(callWhenAuthenticated)
          .catch(this.failAuth);
      }.bind(this));
      return false;
    },

    failAuth: function(err) {
      var state = this.state;
      var error = err.response.body;

      state.error   = error;
      state.waiting = false;
      this.setState(state);
    },

    render: function() {
      var isWait = this.state.waiting;
      var isError = this.state.error;

      var error;
      var waiting;

      if (isError) error = (
        <div className="alert alert-danger alert-dismissible">
          <button type="button" className="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i className="icon fa fa-ban"></i> Atenção!</h4>
          {this.state.error}
        </div>
      );

      if (isWait) waiting = (
        <div className="overlay">
          <i className="fa fa-refresh fa-spin"></i>
        </div>
      );

      return (
        <div className="login-box">
          <div className="login-logo">
            <b>nbA</b>dmin
          </div>

          <div className="box">
            <div className="box-body">
              <div className="login-box-body">
                <p className="login-box-msg">
                  { 'login.label.enter'.translate() }
                </p>

                <form role="form" onSubmit={this.handleTryAuth}>
                  {error}

                  <div className="form-group has-feedback">
                    <input
                      ref="email"
                      type="email"
                      className="form-control"
                      placeholder="Email"
                    />

                    <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>

                  <div className="form-group has-feedback">
                    <input
                      ref="password"
                      type="password"
                      className="form-control"
                      placeholder="Password"
                    />

                    <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>

                  <div className="row">
                    <div className="col-xs-4">
                      <button type="submit" className="btn btn-primary btn-block btn-flat">
                        { 'login.btn.enter'.translate() }
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            {waiting}
          </div>
        </div>
      );
    }
  };
};
