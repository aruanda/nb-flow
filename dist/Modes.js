'use strict';

module.exports = {
  new: 'new',
  edit: 'edit',

  errorNew: 'errorNew',
  errorEdit: 'errorEdit',

  savingNew: 'savingNew',
  savingEdit: 'savingEdit',

  view: 'view',
  viewNew: 'viewNew',
  viewEdit: 'viewEdit',

  isView: function (mode) {
    var modesView = [this.view, this.viewNew, this.viewEdit];
    return Boolean(modesView.indexOf(mode) > -1);
  },

  isForm: function (mode) {
    var modesForm = [this.new, this.edit, this.errorEdit, this.errorNew];
    return Boolean(modesForm.indexOf(mode) > -1);
  },

  isError: function (mode) {
    var modesError = [this.errorEdit, this.errorNew];
    return Boolean(modesError.indexOf(mode) > -1);
  },

  isSaving: function (mode) {
    var modesSaving = [this.savingNew, this.savingEdit];
    return Boolean(modesSaving.indexOf(mode) > -1);
  }
};