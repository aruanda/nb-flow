'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');

gulp.task('copy-json', function() {
  return gulp
    .src(['./src/**/*.json'])
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['copy-json'], function() {
  var babelOptions = {
    plugins: [
      ['transform-react-jsx']
    ]
  };

  return gulp
    .src(['./src/**/*.{jsx,js}'])
    .pipe(babel(babelOptions))
    .pipe(gulp.dest('./dist'));
});
